Date: 2016-01-18  
Title: README  
Published: true  
Type: post  
Excerpt: it  

# README #

[ ![Codeship Status for edplusasu/edplus-not-yet-website](https://codeship.com/projects/165dd770-7e4f-0133-0b37-62403764d7f5/status?branch=master)](https://codeship.com/projects/120296)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Alex Optical
* Version 1.0

### How do I get set up? ###

* All development should be on *master* branch. Clone repo and run 'npm install'.
* Configuration: Gulp (gulpfile.js) and Jekyll (_config.yml) project. Run 'gulp server' and 'gulp watch' on two terminals.
* Dependencies: node and npm.
* How to run tests: gulp test.

### How do I deploy master to test branch ###

* Do [Bitbucket Compare](https://bitbucket.org/edplusasu/edplus-not-yet-website/branches/compare) and merge/sync from *master* to *test* branch.
* The codeship hook will run tests and deploy to [AWS S3 TEST site bucket](http://edu.asu.notyet.test.s3-website-us-west-1.amazonaws.com/) (if setup)

### How do I deploy test to live branch ###

* Similarly, do [Bitbucket Compare](https://bitbucket.org/edplusasu/edplus-not-yet-website/branches/compare) and merge/sync from *test* to *live* branch.
* The codeship hook will run tests and deploy to [AWS S3 LIVE site bucket](http://edu.asu.notyet.live.s3-website-us-west-1.amazonaws.com/) (if setup)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Chandi Cumaranatunge
